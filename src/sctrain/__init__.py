# Importable from root
from .co_training import CoTrainer, SelectionStrategy
from .self_training import CnnSelfTrainer, DnnSelfTrainer

__version__ = '1.0.0'
